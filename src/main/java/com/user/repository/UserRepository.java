package com.user.repository;

import org.springframework.data.repository.CrudRepository;
import com.user.domain.User;

/**
 * Created by tyler on 8/22/16.
 */
public interface UserRepository extends CrudRepository<User, Long>
{
}
