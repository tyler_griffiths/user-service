package com.user.controller;

import com.user.domain.User;
import com.user.exception.ResourceNotFoundException;
import com.user.repository.UserRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.inject.Inject;
import java.net.URI;

/**
 * Created by tyler on 8/22/16.
 */
@RestController
public class UserController
{
    @Inject
    private UserRepository userRepository;

    @RequestMapping(value="/platform/user", method = RequestMethod.GET)
    public ResponseEntity<Iterable<User>> getAllUsers()
    {
        Iterable<User> allUsers = userRepository.findAll();
        return new ResponseEntity<>(allUsers, HttpStatus.OK);
    }

    @RequestMapping(value="/platform/user", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@RequestBody User user)
    {
        user = userRepository.save(user);
        // Set the location header for the newly created user
        HttpHeaders responseHeaders = new HttpHeaders();
        URI newUserUri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(user.getId())
                .toUri();
        responseHeaders.setLocation(newUserUri);

        return new ResponseEntity<>(null, responseHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value="/platform/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable Long userId)
    {
        validateUser(userId);
        User userRef = userRepository.findOne(userId);
        return new ResponseEntity<> (userRef, HttpStatus.OK);
    }

    @RequestMapping(value="/platform/user/{userId}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateUser(@RequestBody User user, @PathVariable Long userId)
    {
        validateUser(userId);
        userRepository.save(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value="/platform/user/{userId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUser(@PathVariable Long userId) {
        validateUser(userId);
        userRepository.delete(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void validateUser(Long userId) throws ResourceNotFoundException
    {
        User user = userRepository.findOne(userId);
        if (user == null)
        {
            throw new ResourceNotFoundException("User not found, invalid ID");
        }
    }

}
